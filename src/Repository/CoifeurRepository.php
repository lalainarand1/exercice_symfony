<?php

namespace App\Repository;

use App\Entity\Coifeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Coifeur>
 *
 * @method Coifeur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coifeur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coifeur[]    findAll()
 * @method Coifeur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoifeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coifeur::class);
    }

//    /**
//     * @return Coifeur[] Returns an array of Coifeur objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Coifeur
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
