<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiseController extends AbstractController
{
    #[Route('/servise', name: 'app_servise')]
    public function index(): Response
    {
        return $this->render('servise/service.html.twig', [
            'controller_name' => 'ServiseController',
        ]);
    }
}
